from flask import Flask
from flask import render_template

app = Flask(__name__)
##app = Flask('0.0.0.0', port=8080, debug=True)


###
import json
prefix="/work/GWOSC/GWTC-1-confident/"
with open ('./static/gwtc1/gwtc1.json') as f:
    jobj = json.load(f)
#events = list( jobj['data'].keys() )

#  for ev in events:
#    #for ev in ['GW151226']:
#    detectors = jobj['data'][ev]['files']['OperatingIFOs'].split()
#    for de in detectors:
#        title = "%s:%s (%d %d)" % (ev, de, deltat, ds) 
#        rawpath = jobj['data'][ev]['files'][de]['4096sec']['4KHz']['HDF']
#        tevent=jobj['data'][ev]['files']['PeakAmpGPS']



@app.route('/')
@app.route('/json')
def index():
    html = render_template('gwtc1.html', title='GWTC-1', events=jobj['data'])
    with open("gwtc1_static.html", "w") as f:
	fp.write(html)
    
    return html



events = list( jobj['data'].keys() )

# Here is an approximate, smoothed PSD for H1 during O1, with no lines. We'll use it later.    
##psd_smooth = interp1d(Afre, np.sqrt(Apsd))




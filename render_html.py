import json
with open ('./static/gwtc1/gwtc1.json') as f:
    jobj = json.load(f)


print('<html><head>')
print("<style>.container{display: flex; flex-direction: row; flex-wrap: nowrap; --align-items: flex-start;}")
print("img {flex: 1; height: 300px; width: auto;}")
print("</style></head>")

print("<body>")
print("<h1><a href=https://www.gw-openscience.org/eventapi/html/GWTC-1-confident/>Catalogue: GWTC-1-confident</a></h1>")


events = sorted( list( jobj['data'].keys() ) )
count=1
for key in events:
    ev = jobj['data'][key]
    m1 = ev["mass1"]["best"]
    m2 = ev["mass2"]["best"]
    snrp = ev["snr_pycbc"]["best"]
    snrg = ev["snr_gstlal"]["best"]
    chi  = ev["chi_eff"]["best"]
    print("<h2>#%2d: %s m=(%s, %s) chi=%s snr=(%s, %s)</h2>" % (count,key, m1,m2, chi, snrp, snrg ))
    count +=1
    for ch in ev['files']['FrameChannels']:
        print("<h3>Channel: %s</h3>" % ch)
        print("  <div class=container>")
        print('  <img src="./static/gwtc1/%s-%s_psd.png">' % (ch[:2], key) )
        print('  <img src="./static/gwtc1/%s-%s_ht.png">' % (ch[:2], key) )
        print('  <img src="./static/gwtc1/%s-%s_wh.png">' % (ch[:2], key) )
        print('</div>')
    print("<hr>")

print('</body></html>')

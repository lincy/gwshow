from __future__ import print_function
import h5py
import json
import numpy as np
import matplotlib as mpl
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
#mpl.rcParams['lines.linewidth'] = 1
mpl.rcParams['figure.figsize'] = 10, 5
mpl.rcParams['font.size'] = 12
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter,detrend, butter, filtfilt
import readligo as rl

#SRATE, SAMPLING=   4096,  "4KHz"
SRATE, SAMPLING= 4*4096, "16KHz"
fs=SRATE
dt=1.0/SRATE


def psd_median(x, Fs, NFFT, window=None, noverlap=0.5):
    dt = 1/Fs
    assert(NFFT%2==0)
    step = int(NFFT*(1-noverlap))
    Ns = int(np.ceil((len(x)-NFFT) / step)) + 1
    #print("[ Tb = %d Ns = %d ]" %(NFFT/Fs, Ns))

    shape, strides = (Ns,NFFT), (8*step,8)
    res = np.lib.stride_tricks.as_strided(x,shape=shape,strides=strides)

    res = detrend(res, axis=-1)
    if window is None:
        window = np.ones(NFFT)
    else:
        res = res*window
        
    res = np.fft.rfft(res, n=NFFT, axis=-1)
    res = np.abs(np.conj(res) * res)

    scale = 2*dt/(np.abs(window)**2).sum() ## 2 handle the one-sided PSD, *dt to make it units of dB/Hz, and correction of
    return np.median(res, axis=0)*scale, np.fft.rfftfreq(NFFT, dt)
    
# function to whiten data
def whiten(strain, interp_psd, dt):
    Nt = len(strain)
    freqs = np.fft.rfftfreq(Nt, dt)
    ###f = np.linspace(0,2048.,Nt/2+1)

    # whitening: transform to freq domain, divide by asd, then transform back, 
    # taking care to get normalization right.
    hf = np.fft.rfft(strain)
    norm = 1./np.sqrt(1./(dt*2))
    white_hf = hf / np.sqrt(interp_psd(freqs)) * norm
    white_ht = np.fft.irfft(white_hf, n=Nt)
    return white_ht


deltat, ds = 128, 16
smallt = 64

fband = [20,800]
NFFT = ds*fs
Afre = np.linspace(0, SRATE/2,int(NFFT/2+1))
Aasd = np.sqrt( (1.e-22*(18./(0.1+Afre))**2)**2 + 0.7e-23**2 + ((Afre/2000.)*4.e-23)**2 )


##### plot PSD
def process_event(ev):
    win = np.blackman(NFFT)
    
    detectors = jobj['data'][ev]['files']['OperatingIFOs'].split()
    for de in detectors:
        title = "%s:%s (%d, %d)" % (ev, de, deltat, ds) 
        rawpath = jobj['data'][ev]['files'][de]['4096sec'][SAMPLING]['HDF']
        tevent=jobj['data'][ev]['files']['PeakAmpGPS']
        localfile = prefix + rawpath.split('/')[-1]
        print(title, localfile)
        
        ### PSD =============================================
        ht, time, _ = rl.loaddata(localfile)
        ht = np.nan_to_num(ht)
        Pxx, freqs = psd_median(ht, Fs = SRATE, NFFT = NFFT, window=win)

        plt.figure(figsize=(8,6))
        plt.tight_layout()
        plt.title(title)
        plt.loglog(freqs, np.sqrt(Pxx))
        plt.loglog(Afre,  Aasd, 'k')
        plt.xlim(10,3000)
        plt.ylim(2e-24,1e-20)
        plt.grid()
        plt.savefig("%s-%s_psd.png"%(de,ev))
        plt.close()

        ### raw h(t) =================
        idx  = np.where((time >= tevent-deltat) & (time < tevent+deltat))
        ivis = np.where((time >= tevent-smallt) & (time < tevent+smallt))
        t = time[ivis]-tevent
        
        ht = np.nan_to_num(ht)
        ht = detrend(ht)
        
        plt.figure()
        plt.title(title)
        plt.plot(t, ht[ivis])
        plt.savefig("%s-%s_ht.png"%(de,ev))
        plt.clf()

        ### wh(t) =================
        ##wht = process(ht, ds=ds, fs=SRATE)
        psdint = interp1d(freqs, Pxx)
        wht = whiten(ht, psdint, 1/fs)

        ### bpass wh(t) =================
        bb, ab = butter(4, [fband[0]*2./fs, fband[1]*2./fs], btype='band')
        normalization = np.sqrt((fband[1]-fband[0])/(fs/2))
        whtbp = filtfilt(bb, ab, wht) / normalization

        plt.title(title)
        plt.plot(t, wht  [ivis], label="wh (%.2f %.2f)" % (wht[ivis].mean(), wht[ivis].std() ) )
        plt.plot(t, whtbp[ivis], label="bp (%.2f %.2f)" % (whtbp[ivis].mean(), whtbp[ivis].std() ) )
        plt.legend()
        plt.savefig("%s-%s_wh.png"%(de,ev))
        print("   Saved %s-%s_wh.png ."%(de,ev))
        plt.close()

import queue
prefix="/work/GWOSC/GWTC-1-confident/"
with open ('gwtc1.json') as f:
    jobj = json.load(f)

events = list( jobj['data'].keys() )
equeue = queue.Queue()
for ev in events:
    equeue.put(ev)


while equeue.qsize() > 0:
   ev = equeue.get()
   print("Work on %s" % ( ev))
   process_event(ev)


print("== Done")






import json
import h5py
import numpy as np
import matplotlib as mpl
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
plt.rcParams['agg.path.chunksize'] = 10000

from gwpy.timeseries import TimeSeries
from gwpy.plot import Plot

if 0:
 #qspecgram = data.q_transform(outseg=(1126259462.2, 1126259462.5))
 plot = qspecgram.plot(figsize=[8, 4])
 ax = plot.gca()
 ax.set_xscale('seconds')
 ax.set_yscale('log')
 ax.set_ylim(20, 500)
 ax.set_ylabel('Frequency [Hz]')
 ax.grid(True, axis='y', which='both')
 ax.colorbar(cmap='viridis', label='Normalized energy')
 plot.savefig("testq.png")
#===================================================

data = TimeSeries.fetch_open_data('H1', 1126259406, 1126259518)

    
hp = data.highpass(4)
white = data.whiten(4, 2, window='hanning', method='median')

plot = Plot(white, separate=True, sharex=True)
plot.savefig("test_wh.png")

plot = Plot(hp, separate=True, sharex=True)
plot.savefig("test.png")


sdfsdfsdff


displacement = hp.zpk([100]*5, [1]*5, 1e-10)
whiteasd = hp.asd(8, 4)
dispasd = displacement.asd(8, 4)

plot = Plot(whiteasd, dispasd, separate=True, sharex=True,
            xscale='log', yscale='log')
            
plot.axes[0].set_ylabel('ASD [whitened]')
plot.axes[1].set_ylabel(r'ASD [m/$\sqrt{\mathrm{Hz}}$]')
plot.axes[1].set_xlabel('Frequency [Hz]')
plot.axes[1].set_ylim(1e-20, 1e-15)
plot.axes[1].set_xlim(5, 4000)
plot.savefig("test.png")









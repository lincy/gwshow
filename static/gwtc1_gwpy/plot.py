from __future__ import print_function
import h5py
import json
import numpy as np
import matplotlib as mpl
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
#mpl.rcParams['lines.linewidth'] = 1
mpl.rcParams['figure.figsize'] = 10, 5
mpl.rcParams['font.size'] = 12
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter,detrend, butter, filtfilt

from gwpy.timeseries import TimeSeries
from gwpy.plot import Plot


#SRATE, SAMPLING=   4096,  "4KHz"
SRATE, SAMPLING= 4*4096, "16KHz"
fs=SRATE
dt=1.0/SRATE

deltat, ds = 128, 16
smallt = 64

fband = [20,800]
NFFT = ds*fs
Afre = np.linspace(0, SRATE/2,int(NFFT/2+1))
Aasd = np.sqrt( (1.e-22*(18./(0.1+Afre))**2)**2 + 0.7e-23**2 + ((Afre/2000.)*4.e-23)**2 )


##### plot PSD
def process_event(ev):
    
    detectors = jobj['data'][ev]['files']['OperatingIFOs'].split()
    for de in detectors:

        title = "%s:%s (%d, %d)" % (ev, de, deltat, ds) 
        rawpath = jobj['data'][ev]['files'][de]['4096sec'][SAMPLING]['HDF']
        tevent=jobj['data'][ev]['files']['PeakAmpGPS']
        localfile = prefix + rawpath.split('/')[-1]
        print(title, localfile)

        #####
        data = TimeSeries.read(localfile, format='hdf5.losc')
        
        plot = data.plot(
            title='LIGO Livingston Observatory data for HW100916',
                ylabel='Strain amplitude',
        )
        
        hp = data.highpass(4)
        displacement = hp.zpk([100]*5, [1]*5, 1e-10)
        whiteasd = data.asd(8, 4)
        
        dispasd = displacement.asd(8, 4)
        
        
        
        plot = Plot(whiteasd, dispasd, separate=True, sharex=True, xscale='log', yscale='log')
        plot.axes[0].set_ylabel('ASD [whitened]')
        plot.axes[1].set_ylabel(r'ASD [m/$\sqrt{\mathrm{Hz}}$]')
        plot.axes[0].set_xlabel('Frequency [Hz]')
        plot.axes[0].set_ylim(1e-24, 1e-18)
        plot.axes[1].set_ylim(1e-34, 1e-18)
        plot.axes[0].set_xlim(5, 4000)
        plot.savefig("aaa.png")
        
        
        ############# Q-transform
        #qspecgram = data.q_transform(outseg=(1126259462.2, 1126259462.5))
        #qspecgram = data.q_transform()
        #plot = qspecgram.plot(figsize=[8, 4])
        #ax = plot.gca()
        #ax.set_xscale('seconds')
        #ax.set_yscale('log')
        #ax.set_ylim(20, 500)
        #ax.set_ylabel('Frequency [Hz]')
        #ax.grid(True, axis='y', which='both')
        #ax.colorbar(cmap='viridis', label='Normalized energy')
        #plot.savefig("q_transform.png")
        
        
        
        sdfsdfsdf

        ### PSD =============================================
        ht, time, _ = rl.loaddata(localfile)
        ht = np.nan_to_num(ht)
        Pxx, freqs = psd_median(ht, Fs = SRATE, NFFT = NFFT, window=win)

        plt.figure(figsize=(8,6))
        plt.tight_layout()
        plt.title(title)
        plt.loglog(freqs, np.sqrt(Pxx))
        plt.loglog(Afre,  Aasd, 'k')
        plt.xlim(10,3000)
        plt.ylim(2e-24,1e-20)
        plt.grid()
        plt.savefig("%s-%s_psd.png"%(de,ev))
        plt.close()

        ### raw h(t) =================
        idx  = np.where((time >= tevent-deltat) & (time < tevent+deltat))
        ivis = np.where((time >= tevent-smallt) & (time < tevent+smallt))
        t = time[ivis]-tevent
        
        ht = np.nan_to_num(ht)
        ht = detrend(ht)
        
        plt.figure()
        plt.title(title)
        plt.plot(t, ht[ivis])
        plt.savefig("%s-%s_ht.png"%(de,ev))
        plt.clf()

        ### wh(t) =================
        ##wht = process(ht, ds=ds, fs=SRATE)
        psdint = interp1d(freqs, Pxx)
        wht = whiten(ht, psdint, 1/fs)

        ### bpass wh(t) =================
        bb, ab = butter(4, [fband[0]*2./fs, fband[1]*2./fs], btype='band')
        normalization = np.sqrt((fband[1]-fband[0])/(fs/2))
        whtbp = filtfilt(bb, ab, wht) / normalization

        plt.title(title)
        plt.plot(t, wht  [ivis], label="wh (%.2f %.2f)" % (wht[ivis].mean(), wht[ivis].std() ) )
        plt.plot(t, whtbp[ivis], label="bp (%.2f %.2f)" % (whtbp[ivis].mean(), whtbp[ivis].std() ) )
        plt.legend()
        plt.savefig("%s-%s_wh.png"%(de,ev))
        print("   Saved %s-%s_wh.png ."%(de,ev))
        plt.close()

import queue
prefix="/work/GWOSC/GWTC-1-confident/"
with open ('gwtc1.json') as f:
    jobj = json.load(f)

events = list( jobj['data'].keys() )
equeue = queue.Queue()
for ev in events:
    equeue.put(ev)


while equeue.qsize() > 0:
   ev = equeue.get()
   print("Work on %s" % ( ev))
   process_event(ev)


print("== Done")






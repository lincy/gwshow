from __future__ import print_function
import h5py
import numpy as np
import matplotlib as mpl
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
#mpl.rcParams['lines.linewidth'] = 1
mpl.rcParams['figure.figsize'] = 10, 5
mpl.rcParams['font.size'] = 12
##mpl.rcParams['agg.path.chunksize'] = 10000
from scipy.interpolate import interp1d
import readligo as rl


from scipy.signal import savgol_filter,detrend, butter, filtfilt
from scipy.interpolate import interp1d

import json
SRATE=4096
fs=SRATE
dt=1.0/SRATE

def psd_median(x, Fs, NFFT, window=None, noverlap=0.5):
    dt = 1/Fs
    assert(NFFT%2==0)
    step = int(NFFT*(1-noverlap))
    Ns = int(np.ceil((len(x)-NFFT) / step)) + 1
    #print("[ Tb = %d Ns = %d ]" %(NFFT/Fs, Ns))

    shape, strides = (Ns,NFFT), (8*step,8)
    res = np.lib.stride_tricks.as_strided(x,shape=shape,strides=strides)

    res = detrend(res, axis=-1)
    if window is None:
        window = np.ones(NFFT)
    else:
        res = res*window
        
    res = np.fft.rfft(res, n=NFFT, axis=-1)
    res = np.abs(np.conj(res) * res)

    scale = 2*dt/(np.abs(window)**2).sum() ## 2 handle the one-sided PSD, *dt to make it units of dB/Hz, and correction of
    return np.median(res, axis=0)*scale, np.fft.rfftfreq(NFFT, dt)
    
# function to whiten data
def whiten(strain, interp_psd, dt):
    Nt = len(strain)
    freqs = np.fft.rfftfreq(Nt, dt)
    ###f = np.linspace(0,2048.,Nt/2+1)

    # whitening: transform to freq domain, divide by asd, then transform back, 
    # taking care to get normalization right.
    hf = np.fft.rfft(strain)
    norm = 1./np.sqrt(1./(dt*2))
    white_hf = hf / np.sqrt(interp_psd(freqs)) * norm
    white_ht = np.fft.irfft(white_hf, n=Nt)
    return white_ht

def process(x, ds=64, fs=SRATE):
    NFFT = ds*fs
    win = np.blackman(NFFT)
    #Pxx_H1, freqs    = mlab.psd(strain_H1[indxt], Fs = fs, NFFT = NFFT, window=win, noverlap=NFFT//2)
    Pxx, freqs = psd_median(x, Fs = fs, NFFT = NFFT, window=win)

    psdint = interp1d(freqs, Pxx)
    htw = whiten(x, psdint, 1/fs)
    return htw
    

prefix="./raw/"
prefix="/work/GWOSC/GWTC-1-confident/"
with open ('gwtc1.json') as f:
    jobj = json.load(f)

deltat, ds = 128, 16
smallt = 64
events = list( jobj['data'].keys() )

# Here is an approximate, smoothed PSD for H1 during O1, with no lines. We'll use it later.    
##psd_smooth = interp1d(Afre, np.sqrt(Apsd))


def pp(x,y,fname,title=' '):
    plt.title(title)
    plt.plot(x, y)
    plt.savefig(fname)
    plt.clf()


##### plot PSD
if 1:
  NFFT = ds*fs
  Afre = np.linspace(0, 2048.,int(NFFT/2+1))
  Aasd = np.sqrt( (1.e-22*(18./(0.1+Afre))**2)**2 + 0.7e-23**2 + ((Afre/2000.)*4.e-23)**2 )
  win = np.blackman(NFFT)
  plt.figure(figsize=(8,6))
  plt.tight_layout()
  for ev in events:
    #for ev in ['GW151226']:
    detectors = jobj['data'][ev]['files']['OperatingIFOs'].split()
    for de in detectors:
        title = "%s:%s (%d, %d)" % (ev, de, deltat, ds) 
        rawpath = jobj['data'][ev]['files'][de]['4096sec']['4KHz']['HDF']
        tevent=jobj['data'][ev]['files']['PeakAmpGPS']
        localfile = prefix + rawpath.split('/')[-1]
        print(title, localfile)
        
        ht, _, _ = rl.loaddata(localfile)
        ht = np.nan_to_num(ht)
        Pxx, freqs = psd_median(ht, Fs = SRATE, NFFT = NFFT, window=win)

        plt.title(title)
        plt.loglog(freqs, np.sqrt(Pxx))
        plt.loglog(Afre,  Aasd, 'k')

        plt.xlim(10,2000)
        plt.ylim(2e-24,1e-20)
        plt.grid()
        plt.savefig("%s-%s_psd.png"%(de,ev))
        plt.clf()
  plt.close()

###################
fband = [20,800]
if 1:
  plt.figure(figsize=(8,3))
  plt.tight_layout()
  for ev in events:
    ##for ev in ['GW150914']:
    detectors = jobj['data'][ev]['files']['OperatingIFOs'].split()
    for de in detectors: 
        title = "%s:%s (%d %d)" % (de,ev, deltat, ds) 
        print(title)

        rawpath = jobj['data'][ev]['files'][de]['4096sec']['4KHz']['HDF']
        tevent=jobj['data'][ev]['files']['PeakAmpGPS']
        localfile = prefix + rawpath.split('/')[-1]
        
        ht, time, _ = rl.loaddata(localfile)
        idx  = np.where((time >= tevent-deltat) & (time < tevent+deltat))
        ivis = np.where((time >= tevent-smallt) & (time < tevent+smallt))
        t = time[ivis]-tevent
        
        ###=================
        ht = np.nan_to_num(ht)
        ht = detrend(ht)
        pp(t, ht[ivis], "%s-%s_ht.png"%(de,ev), title)

        ###=================
        wht = process(ht, ds=ds, fs=SRATE)
        bb, ab = butter(4, [fband[0]*2./fs, fband[1]*2./fs], btype='band')
        normalization = np.sqrt((fband[1]-fband[0])/(fs/2))
        whtbp = filtfilt(bb, ab, wht) / normalization

        plt.title(title)
        plt.plot(t, wht  [ivis], label="wh (%.2f %.2f)" % (wht[ivis].mean(), wht[ivis].std() ) )
        plt.plot(t, whtbp[ivis], label="bp (%.2f %.2f)" % (whtbp[ivis].mean(), whtbp[ivis].std() ) )
        plt.legend()
        plt.savefig("%s-%s_wh.png"%(de,ev))
        plt.clf()

  plt.close()



def get_tblock(gpstime, cache, detector = 'H1', flag='CBC_CAT3', tblock=2048):
    """
    Read segments from cache
    FLAG='DATA' | 'CBC_CAT2' | 'CBC_CAT3'
    """
    TE = int(np.round( gpstime ))
    TD = tblock//2
    t0, t1 = TE-TD, TE+TD

    filelist = rl.FileList(cache=cache)

    print("--- Searching %4d block between t=(%d,%d):" % (t1-t0, t0, t1))
    segList  = rl.getsegs(t0, t1, detector, flag=flag, filelist=filelist)
    print(segList)

    out = []
    for start, stop in segList:
        duration = stop-start
        if (duration < tblock):
            print("     This block short than TBLOCK=%ds. Set TBLOCK=%ds." % (tblock, duration))
            TBLOCK = duration
        print("--- Processed segment : {0} sec.".format(duration))

        ## out strain, meta, dq
        out.append( rl.getstrain(start, stop, detector, filelist=filelist) )
    return out

def scan_gwtc1(tblock=1024, FLAG = 'CBC_CAT3'):
    detectors = ['H1', 'L1', 'V1']
    filelist = rl.FileList(cache=CACHE)
    nevent = len(evname)

    for i in range(nevent):
        gpstime = evgps[i]
        print("===== Event #%2d: %s at %f" % (i, evname[i], gpstime))

        TE = int(np.round( gpstime ))
        TD = tblock//2
        t0, t1 = TE-TD, TE+TD

        for d in detectors:
            print("      %s : searching block %4ds between t=(%d,%d)" % (d, t1-t0, t0, t1))
            segList  = rl.getsegs(t0, t1, d, flag=FLAG, filelist=filelist)
            for seg in segList:
                print ("From %10f to %10f" % ( seg[0]-gpstime, seg[1]-gpstime) )

def plot_gwtc1(tblock=256, FLAG = 'CBC_CAT3'):
    detectors = ['H1', 'L1', 'V1']
    filelist = rl.FileList(cache=CACHE)
    nevent = len(evname)

    for i in range(nevent):
        gpstime = evgps[i]
        print("===== Event #%2d: %s at %f" % (i, evname[i], gpstime))

        TE = int(np.round( gpstime ))
        TD = tblock//2
        t0, t1 = TE-TD, TE+TD

        for d in detectors:
            print("      %s : searching block %4ds between t=(%d,%d)" % (d, t1-t0, t0, t1))
            segList  = rl.getsegs(t0, t1, d, flag=FLAG, filelist=filelist)
            for seg in segList:
                print ("From %10f to %10f" % ( seg[0]-gpstime, seg[1]-gpstime) )





